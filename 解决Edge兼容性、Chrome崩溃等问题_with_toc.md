<a name="index">**Index**</a>
&emsp;<a href="#0">Chrome浏览器</a>  
&emsp;<a href="#1">Edge浏览器</a>  
&emsp;&emsp;<a href="#2">常见问题</a>  

## <a name="0">Chrome浏览器</a><a style="float:right;text-decoration:none;" href="#index">[Top]</a>

Chrome快捷方式右键打开属性，在.exe之后，先空一格，再复制粘贴-no-sandbox。重启Chrome后即可恢复正常使用，但不保证能打开PDF。且Chrome会提示有sandbox模式下有安全性问题。



## <a name="1">Edge浏览器</a><a style="float:right;text-decoration:none;" href="#index">[Top]</a>

![Edge](.assets/Edge.jpg)

Edge有新版旧版之分，由于新版用的是Chrome内核，所以存在与Chrome浏览器的共性问题。

Edge浏览器快捷方式右键打开属性，在.exe后面的`”`之后，先空一格，再复制粘贴-no-sandbox。重启Edge浏览器后即可恢复正常使用，但不保证能打开PDF。且Edge浏览器会提示有sandbox模式下有安全性问题。

## <a name="2">常见问题</a><a style="float:right;text-decoration:none;" href="#index">[Top]</a>

![img](.assets/9b40ed95-7a27-4d58-afbc-536a65789042.jpg)

`-no-sandbox`字符串位置加错了，应在`“`的右边加。

