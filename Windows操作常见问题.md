任务栏搜索框

[Win10任务栏搜索框怎么开启或者禁用？_windows10_Windows系列_操作系统_脚本之家 (jb51.net)](https://www.jb51.net/os/win10/302797.html)



[清除 Internet Explorer 浏览器缓存 (qq.com)](https://ctc.qzs.qq.com/qzone/vas/app/app_canvas/browser/ie.htm)



> # 任务管理器里不见了磁盘一项
>
> ![图片](https://filestore.community.support.microsoft.com/api/images/d155047d-fe84-4646-aed3-5995f598e9cd)
>
> 如图，之前是有的！

检查是否接了外置存储。有可能是外置存储不正常导致的。且这种情况，外置存储也无法弹出。

解决办法：直接把外置存储拔了。就正常了。磁盘管理器也可以打开了。
