![Wer](.assets/Wer.jpg)

现象：电脑平常使用时，安装Rhino时，报图片中的错误。

解决办法：

管理员运行cmd （命令行）

  1.输入 for %1 in (%windir%\system32\*.dll) do regsvr32.exe /s %1

  2输入 for %1 in (%windir%\system32\*.ocx) do regsvr32.exe /s %1

  3.重启电脑。

第一次重启电脑过程中可能会进行Windows更新，重启之后可能会继续报图片中的错误。只要再次重启电脑，即可消除报错。
此方法也不是万能的。成功过一次，失败过一次。但相比于sfcscan，内存检测，成功率要高。