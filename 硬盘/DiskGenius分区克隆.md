本教材讲解的是** 按文件复制**：通过分析源分区(卷)中的文件数据组织结构，将源分区中的所有文件复制到目标分区(卷)。复制时会将目标分区中的文件按文件系统结构的要求重新组织。用此方式复制后，目标分区将没有文件碎片，复制分区速度也比较快。此方式不要求目标分区的容量必须与源分区相同，只要大于源分区的已用数据总量即可。

1. 选择需要克隆的分区，然后点击菜单“工具 - 克隆分区”项，也可以从右键菜单中选择“克隆分区”菜单项。
![克隆分区](vx_images/289905215248990.png)
2. 程序弹出“克隆分区”对话框，选择目标分区，然后点击“确定”按钮。
![选择](vx_images/164625315236857.png)

3. 选择好目标分区后，重新回到克隆分区对话框。点击“开始”按钮准备克隆分区。
提醒：软件提供三种克隆方式，但是如果两个分区的大小不相同，将只能选择“按文件复制”。
![源、目标选择](vx_images/572945315257023.png)

错误示例
![错误示例](vx_images/130715115230564.png)
图中错误地将源、目标选择了同一分区。源分区应该选择拷贝的分区，目标分区应该选择粘贴的分区。
4. 程序显示下面的警告提示，确认无误后点击“确定”按钮。
![提示覆盖](vx_images/182465415249692.png)

5. 选择完成分区克隆的执行方式。
![提示](vx_images/367725415246247.png)

6. 程序开始克隆，等待操作完成即可。
![克隆分区](vx_images/528545415242001.png)
“按文件”克隆时，如果源分区没有锁定，克隆分区完成后，程序将自动对克隆后的目标分区数据做必要的检查及更正。
