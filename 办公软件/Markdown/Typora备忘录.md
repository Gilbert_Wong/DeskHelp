> 什么是显示当前块元素的markdown源码`Display source for simple block on focus` should maintain style on focus？

![image-20220215175539089](.assets/image-20220215175539089.png)

### enable

![image-20220215175609784](.assets/image-20220215175609784.png)

当你把光标放在标记语言上，会显示源码

### disable

![image-20220215175745922](.assets/image-20220215175745922.png)

当你把光标放在标记语言上，不显示源码，而是保持渲染后的效果，

