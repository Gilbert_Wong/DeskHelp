使用Office Tool Plus安装激活Office，必须安装(Volume)版本，不能安装版本，否则无法使用Office Tool Plus激活。

> 报错：Office无法安装在指定通道上：测试通道

解决办法：切换安装版本的对应通道即可。
![更新通道](vx_images/456501818230344.png)