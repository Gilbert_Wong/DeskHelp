=COUNTIF(A:A,A1)=1

A1即代表设置列中的第一个cell。**第一个cell要和你实际选中的第一个cell是同一个。**

COUNTIF计算某个区域中满足给定条件的单元格数目。

Explanation: The [COUNTIF function](https://www.excel-easy.com/examples/countif.html) takes two arguments. =COUNTIF($A$2:$A$20,A2) counts the number of values in the range A2:A20 that are equal to the value in cell A2. This value may only occur once (=1) since we don't want duplicate entries. Because we selected the range A2:A20 before we clicked on Data Validation, Excel automatically copies the formula to the other cells.