我们需要先打开Microsoft Office Outlook(微软邮箱)，单击左上角“文件”，再单击左侧“选项”;

![1.png](https://www.office26.com/uploads/allimg/200310/15512311B-0.png)

然后我们选择“邮件”---->再单击“签名”按钮

![2.png](https://www.office26.com/uploads/allimg/200310/1551234252-1.png)

![img](file:///C:/Users/Gilbert/AppData/Local/Temp/msohtmlclip1/01/clip_image002.jpg)

 

之后我们新建或者答复邮件时，邮件末端就会自动添加签名了。

![img](file:///C:/Users/Gilbert/AppData/Local/Temp/msohtmlclip1/01/clip_image004.jpg)