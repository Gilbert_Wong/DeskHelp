## 问题

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210422103958939.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMyNjgyMzAx,size_16,color_FFFFFF,t_70)

## 解决

需要在控制方电脑操作。

win+R，打开运行窗口，输入regedit，打开注册表编辑器

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210422104445141.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMyNjgyMzAx,size_16,color_FFFFFF,t_70)

2.

找到路径：计算机\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System
在System文件夹内创建文件夹项：\CredSSP\Parameters
在Parameters文件夹内，新建 DWORD（32）位值（D），文件名为AllowEncryptionOracle，值为2

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210422104352961.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMyNjgyMzAx,size_16,color_FFFFFF,t_70)


重新远程连接一下，即可正常连接。


原文链接：https://blog.csdn.net/qq_32682301/article/details/116003700

