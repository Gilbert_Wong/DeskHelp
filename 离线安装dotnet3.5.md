## 说明

win10默认不包含.net3.5.

## 操作

首先在Win10 ISO文件上点击右键，选择“装载”。如图：

[![Win10离线安装.NET Framework 3.5的方法技巧（附离线安装包下载）](https://www.windows10.pro/wp-content/uploads/2014/10/2014-10-06_214233.png)](https://www.windows10.pro/wp-content/uploads/2014/10/2014-10-06_214233.png)

“这台电脑”中就会显示虚拟光驱“DVD驱动器(L:)”，记下盘符“L:”。

然后[以管理员身份运行命令提示符](https://www.windows10.pro/win10-command-prompt-run-as-administrator/) （或者右键点击Win10开始按钮，以管理员身份运行Windows PowerShell），在“管理员:命令提示符（或管理员: Windows PowerShell）”窗口中输入以下命令：

> dism.exe /online /enable-feature /featurename:netfx3 /Source:L:\sources\sxs

注：其中的盘符L要改成你实际的虚拟光驱盘符

[![Win10离线安装.NET Framework 3.5的方法技巧（附离线安装包下载）](https://www.windows10.pro/wp-content/uploads/2014/10/2014-10-06_220246.png)](https://www.windows10.pro/wp-content/uploads/2014/10/2014-10-06_220246.png)

在“管理员: 命令提示符”中运行命令

![Win10离线安装.NET Framework 3.5的方法技巧](https://www.windows10.pro/wp-content/uploads/2014/10/2017-03-24_091333.png)

在“管理员: Windows PowerShell”中运行命令

回车，等待部署完毕，进度100%，提示“操作成功完成”。

这样即完成了Win10安装.NET Framework 3.5。

验证：你可以[打开控制面板](https://www.windows10.pro/how-to-open-win10-control-panel/)，进入“程序”，点击“程序和功能”下的“启用或关闭Windows功能”。如图：

[![Win10离线安装.NET Framework 3.5的方法技巧（附离线安装包下载）](https://www.windows10.pro/wp-content/uploads/2014/10/2014-10-06_224019.png)](https://www.windows10.pro/wp-content/uploads/2014/10/2014-10-06_224019.png)

然后在打开的“Windows功能”窗口中查看“.NET Framework 3.5(包括.NET 2.0和3.0)”，如果已经被选中，则证明已经安装成功。如图：

[![Win10离线安装.NET Framework 3.5的方法技巧（附离线安装包下载）](https://www.windows10.pro/wp-content/uploads/2014/10/2014-10-06_224120.png)](https://www.windows10.pro/wp-content/uploads/2014/10/2014-10-06_224120.png)

