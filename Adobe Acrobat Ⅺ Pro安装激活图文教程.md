**Adobe Acrobat** **Ⅺ** **Pro**安装激活图文教程
不确定&教程未提及的选项，保持默认即可，无需更改。
1、注意一定要断网安装。最晚到哪一步需要断网，我不清楚。**故建议从安装开始前，就要断网，一直到破解结束。**

 

2、将AcrobatPro_11_Web_WWMUI.exe解压到一个目录下，找到目录下的setup.exe安装，安装时选择“使用试用版本或订阅”。

![img](.assets/wpsD516.tmp.png) 

 ![image-20220223112015583](.assets/image-20220223112015583.png)
 Generate=生成
 为了防止意外，丢失序列号，导致重新安装软件。建议您生成序列号后，粘贴到任意一个文本文件中。即使意外丢失序列号，也能找回来，而不必重复安装步骤。

![image-20220223112052600](.assets/image-20220223112052600.png)
破解工具原理：
1. 破解工具生成序列号
2. Adobe Acrobat根据电脑参数生成请求码
3. 破解工具根据序列号、请求码，计算出激活码。

![image-20220223112137644](.assets/image-20220223112137644.png)
用户名保持系统默认即可，无需更改。
将破解工具生成的Serial(序列号)粘贴至Adobe Acrobat 序列号的方框内。

![image-20220223112217650](.assets/image-20220223112217650.png)

直接点击下一步

![image-20220223112237540](.assets/image-20220223112237540.png)

看不懂自定义、完整的话，默认典型即可。

![image-20220223112418172](.assets/image-20220223112418172.png)

此步为可选项，可更改安装路径，也可以选择不更改。但是建议您不要什么都装在C盘。

![image-20220223112504468](.assets/image-20220223112504468.png)

3、根据你喜好选择“典型”、“完整”、“自定义”三种安装类型，前两种不可以自定义组件，只能更改安装路径；“自定义”安装则组件和安装路径都可以更改。还有就是注意安装过程中不要运行Office程序。

![img](.assets/wpsD527.tmp.png) 

![img](.assets/wpsD528.tmp.png) 

![img](.assets/wpsD529.tmp.jpg)

![image-20220223112631975](.assets/image-20220223112631975.png)

到这一步就安装完成了，接下来是破解。点击完成，或右上角关闭即可。

 

![img](.assets/wpsD52A.tmp.png) 

![img](.assets/wpsD52B.tmp.png) 

 

4、安装完毕首次运行软件，如果是用防火墙拦截的话，凡是该软件弹出的联网请求全部拒绝（下同）。

![img](.assets/wpsD52C.tmp.png) 

![img](.assets/wpsD52D.tmp.png) 

 

5、在弹出的Adobe软件许可协议窗口里选择“接受”，在Adobe Acrobat XI Pro30天试用版窗口里，选择“对此软件进行许可”，输入之前的序列号：1118-1768-4702-6801-1059-4923

（**注意：这个序列号就是用** **xf-aarpxi.exe****算出来的，你也可以不用我这个序列号，自己运行****xf-aarpxi.exe****算也一样。****输入后要记住，因为后面脱机激活还要用到。**）

![img](.assets/wpsD53D.tmp.png) 

![img](.assets/wpsD53E.tmp.png) 

![img](.assets/wpsD53F.tmp.png) 
若前面的安装过程填写过了序列号，则不会出现这一步骤。

![img](.assets/wpsD540.tmp.png) 

![img](.assets/wpsD541.tmp.png) 

 

6、因为是断网状态，所以肯定联网不成功，等到弹出“请连接到Internet，然后重试”窗口时选择“稍后连接”并关闭程序。

![img](.assets/wpsD542.tmp.png) 

 

7、再次打开软件，稍等一会就会弹出“正在验证序列号。这可能需要几秒钟时间…”的窗口。

（使用防火墙拦截才会出现此窗口，如果是直接断网，就不会弹出，也不用等待那么长时间）

等到问你“连接到Internet时是否出现问题？”，点击它。

![img](.assets/wpsD553.tmp.png) 

![img](.assets/wpsD554.tmp.png) 

 

8、点击“脱机激活”并接着点击“生成请求代码”。

![img](.assets/wpsD555.tmp.png) 

![img](.assets/wpsD556.tmp.png) 

![img](.assets/wpsD557.tmp.png) 

 

9、不要关闭上面窗口，再去运行xf-aarpxi.exe

(1)第一行输入之前你输入的序列号；

(2)第二行输入上面窗口里的“请求代码”；

(3)点击GENERATE生成“响应代码”，复制到上面的脱机激活窗口的响应代码区；

(4)点击“激活”并确认看到“脱机激活完成”的信息。

![img](.assets/wpsD558.tmp.png) 

![img](.assets/wpsD569.tmp.png) 

![img](.assets/wpsD56A.tmp.png) 



### (内网机不必看)屏蔽联网验证

10、脱机激活后在hosts文件里添加（注意：前两行是一定要的，后面行是为了保险起见）

127.0.0.1 lmlicenses.wip4.adobe.com

127.0.0.1 lm.licenses.adobe.com

127.0.0.1 3dns-2.adobe.com

127.0.0.1 3dns-3.adobe.com

127.0.0.1 activate.adobe.com

127.0.0.1 activate-sea.adobe.com

127.0.0.1 activate-sjc0.adobe.com

127.0.0.1 adobe-dns.adobe.com

127.0.0.1 adobe-dns-2.adobe.com

127.0.0.1 adobe-dns-3.adobe.com

127.0.0.1 ereg.adobe.com

127.0.0.1 hl2rcv.adobe.com

127.0.0.1 practivate.adobe.com

127.0.0.1 wip3.adobe.com

127.0.0.1 activate.wip3.adobe.com

127.0.0.1 ereg.wip3.adobe.com

127.0.0.1 wwis-dubc1-vip60.adobe.com

或者使用破解的amtlib.dll文件替换C:\Program Files\Adobe\Acrobat 11.0\Acrobat下的同名文件

用以屏蔽联网验证。

**注意：****红色部分****为默认路径，请查看自己实际安装路径。**

**注意：破解的****amtlib.dll****仅仅是屏蔽联网验证，和上面修改hosts文件的作用是一样的，它并不是帮你激活软件，只是防止你的激活被验证出来非正版，防止反弹而已。**

 

11、运行软件，编辑——首选项——更新程序，选择“不自动下载或安装更新”

话说咱都是直接去http://www.adobe.com/support/downloads/product.jsp?product=1&platform=Windows下载Adobe Acrabat的完整更新程序来安装。

![img](.assets/wpsD56B.tmp.png) 

 

12、安装及使用过程中可能会弹出4个不同程序文件的联网请求，分别是

C:\Program Files\Common Files\Adobe\OOBE\PDApp\core\PDapp.exe（这个文件就是软件激活之前运行时弹出的窗口的进程，当然要防火墙拦截）

C:\Program Files\Adobe\Acrobat 11.0\Acrobat\Acrobat.exe（这个是主程序文件，反正这软件也不联网使用，也防火墙拦截吧）

C:\Program Files\Common Files\Adobe\OOBE\PDApp\P6\adobe_licutil.exe（这个就是联网验证序列号的程序，肯定要防火墙拦截）

C:\Program Files\Common Files\Adobe\ARM\1.0\AdobeARM.exe（这个是检查更新的程序，根据自己需要设置防火墙放行还是拦截）

**注意：****红色部分****为默认路径，请查看自己实际安装路径。**

![img](.assets/wpsD56C.tmp.png) 

 

13、最后开始清理工作：

(1)去注册表HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run下删除

Acrobat Assistant 8.0

Adobe ARM

AdobeAAMUpdater-1.0

这3个生成的启动项。

![img](.assets/wpsD56D.tmp.png) 

注意：以后每次打补丁还会生成这些启动项，记得要去删除。

(2)软件还会在Google Chrome浏览器里添加“Adobe Acrobat - 创建 PDF 11.0.0.379”这个扩展，作用是“将网页转换为 PDF”，如果不需要，可以删除。

如果以后还想安装这个扩展，去C:\Program Files\Adobe\Acrobat 11.0\Acrobat\Browser\WCChromeExtn下找到WCChromeExtn.crx这个文件拖到chrome扩展程序界面安装即可。

![img](.assets/wpsD56E.tmp.png) 

(3) 去除右键菜单里Adobe PDF相关项目：

反注册ContextMenuShim.dll（32位系统）或ContextMenuShim64.dll（64位系统）即可

regsvr32 /u /s "C:\Program Files\Adobe\Acrobat 11.0\Acrobat Elements\ContextMenuShim.dll"

regsvr32 /u /s "C:\Program Files\Adobe\Acrobat 11.0\Acrobat Elements\ContextMenuShim64.dll"

**注意：****红色部分****为默认路径，请根据自己实际安装路径修改。**

![img](.assets/wpsD57E.tmp.png) 

(4)软件在运行时可能会出现多个进程，主程序关闭后，仍可能会有一个或多个进程残余，这些文件也不能删除或改名，否则会影响软件功能。

只能自己手工结束这些进程吧，编辑个批处理会稍微方便些，每次关闭程序后运行一下：

@echo off

TITLE 清理Adobe Acrobat XI Pro 残余进程

taskkill /im AcroDist.exe /f

taskkill /im AcroTray.exe /f

taskkill /im adobe_licutil.exe /f

exit

 

 

**结语：文中出现的系统目录全部以Windows XP为基准，其它Windows系统如不一致请自行更正。**

**文中提到的AcrobatPro_11_Web_WWMUI.exe文件、xf-aarpxi.exe****、amtlib 6.2.0.42 破解(屏蔽联网验证)、****还有本文教程全部可以在****百度****网盘下载到：**

[**http://pan.baidu.com/share/link?shareid=105209&uk=3389385845**](http://pan.baidu.com/share/link?shareid=105209&uk=3389385845)

